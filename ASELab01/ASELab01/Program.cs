﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASELab01
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Starting....");
            Program app = new Program();
            app.go();
        }

        public void go()
        {
            Console.WriteLine("in go");

            MessageBox.Show("Message", "Title", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            Console.WriteLine("Insert your weight in kilograms");
            string weight = Console.ReadLine();
            float fweight = Single.Parse(weight);

            Console.WriteLine("Insert your height in meters");
            string height = Console.ReadLine();
            float fheight = Single.Parse(height);


            float bmi = fweight / (fheight * fheight);

            string sbmi = bmi.ToString();

            Console.WriteLine(sbmi);

            Console.ReadLine();
        }
    }
}
